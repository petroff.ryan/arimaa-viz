#!/usr/bin/env node

// This is the arimaa console visualizer
// it is a simple tool for one purpose
// it accepts a standard arimaa board state (position)
// and outputs a graphical representation of the board state
// with the magic of emoji! arimaaji! :)
/*
https://github.com/Janzert/AEI/blob/master/aei-protocol.txt
<position>  a board consisting of ... a piece letter or a space for each of
the 64 squares from left to right and top to bottom (a8-h8, a7-h7...a1-h1).
*/

var arimaaviz = function () {
  var colors = require('colors');
  var gameToVisualize = `r r rrrrdhcemch rr       d                      DHCMECHDRRRRRRRR`;
  if (typeof process.argv[2] === 'string' && process.argv[2].length == 64) {
      console.log(process.argv[2]);
      gameToVisualize = process.argv[2];
    } else {
      console.log("Something's fishy. Enjoy my default output! Also, this might help debug:")
      console.log(process.argv);
      console.log("Try putting in a valid 64 char string?")
  }

  var nl = `
  `;
  var pieces = {
    x : { //⠀blank⠀square⠀⠀⠀⠀⠀⠀⠀⠀⠀
      t : `⠀⠀⠀⠀⠀`,
      m : `⠀⠀⠀⠀⠀`,
      b : `⠀⠀⠀⠀⠀`
    },
    X : {
      t : `↘ ⬇ ↙`.cyan,
      m : `➡ 🍥 ⬅`.cyan,
      b : `↗ ⬆ ↖`.cyan
    },
    e : {
      t : `⢀⣤⣤⣤⡀`.red, //top
      m : `⣿ `.red + `🐘`.bold.red + ` ⣿`.red, //middle
      b : `⠈⠛⠛⠛⠁`.red  //bottom
    },
    m : {
      t : `⢀⢀⣀⣀ `.red,
      m : `⢸ 🐫 ⡇`.red,
      b : ` ⠉⠉⠁⠁`.red
    },
    h : {
      t : ` ⢀⣀⡀ `.red,
      m : `⢸ 🐴 ⡇`.red,
      b : ` ⠈⠉⠁ `.red
    },
    d : {
      t : `  ⣀⡀ `.red,
      m : `⠸ 🐶 ⡆`.red,
      b : ` ⠈⠉  `.red
    },
    c : {
      t : `  ⣀  `.red,
      m : `⠰ 😸 ⠆`.red,
      b : `  ⠉  `.red
    },
    r : {
      t : `  ⢀  `.red,
      m : `⠐ 🐰 ⠄`.red,
      b : `  ⠁  `.red
    },
    E : {
      t : `⢀⣤⣤⣤⡀`.yellow, //top
      m : `⣿ `.yellow + `🐘`.bold.yellow + ` ⣿`.yellow, //middle
      b : `⠈⠛⠛⠛⠁`.yellow  //bottom
    },
    M : {
      t : `⢀⢀⣀⣀ `.yellow,
      m : `⢸ 🐫 ⡇`.yellow,
      b : ` ⠉⠉⠁⠁`.yellow
    },
    H : {
      t : ` ⢀⣀⡀ `.yellow,
      m : `⢸ 🐴 ⡇`.yellow,
      b : ` ⠈⠉⠁ `.yellow
    },
    D : {
      t : `  ⣀⡀ `.yellow,
      m : `⠸ 🐶 ⡆`.yellow,
      b : ` ⠈⠉  `.yellow
    },
    C : {
      t : `  ⣀  `.yellow,
      m : `⠰ 😸 ⠆`.yellow,
      b : `  ⠉  `.yellow
    },
    R : {
      t : `  ⢀  `.yellow,
      m : `⠐ 🐰 ⠄`.yellow,
      b : `  ⠁  `.yellow
    }
  };

  var renderBoard = function (board) {
    // background colours are added in RENDERING
    var populateFullPieceArray = function (piece64chars) {
      var pieceArray = [];
      var odd = 0;
      for (i=0;i<64;i++) {
        if (piece64chars[i] != ` `) {
          pieceArray.push(pieces[piece64chars[i]]);
      } else if (piece64chars[i] == ` `) {
          if (i == 18 || i == 45 || i == 21 || i == 42) {
              pieceArray.push(pieces.X);
            } else {
              pieceArray.push(pieces.x);
          };
        } else {
          console.log("errorr");
        }
      }
      return pieceArray;
    };
    var renderLines = function (pieceArray) {
      var renderString = `` + nl;
      var tString = ``;
      var mString = ``;
      var bString = ``;
      var odd = 0;

      for (i=0;i<64;i++) {
        if (i == 18 || i == 45) {
          tString += pieceArray[i].t.bgWhite;
          mString += pieceArray[i].m.bgWhite;
          bString += pieceArray[i].b.bgWhite;
        } else if (i == 21 || i == 42) {
          tString += pieceArray[i].t.bgBlack;
          mString += pieceArray[i].m.bgBlack;
          bString += pieceArray[i].b.bgBlack;
        } else {
          if ((i + odd) % 2) {
            tString += pieceArray[i].t.bgWhite;
            mString += pieceArray[i].m.bgWhite;
            bString += pieceArray[i].b.bgWhite;
          } else {
            tString += pieceArray[i].t.bgBlack;
            mString += pieceArray[i].m.bgBlack;
            bString += pieceArray[i].b.bgBlack;
          }
        }
        if (i == 7 || i == 15 || i == 23 || i == 31 || i == 39 || i == 47 || i == 55 || i == 63) {
          renderString += tString + nl + mString + nl + bString + nl;
          odd = !odd;
          tString = ``;
          mString = ``;
          bString = ``;
        }
      }
      return renderString;
    };

    return renderLines(populateFullPieceArray(board));
  };

  console.log(renderBoard(gameToVisualize));
};

module.exports = arimaaviz;

arimaaviz();
