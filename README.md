# ARIMAA-VIZ

Welcome to the wonderful world of Arimaa-viz! 
Arimaa-viz is for visualizing Arimaa games!
Arimaa-viz was designed to accept a 64-char string and output a board image.
Here's an example: (After `npm install -g arimaa-viz`, of course.)
```
arimaa-viz 'rrrrrrrrcdemdchh                                RDHEMHDRCCRRRRRR'
```
will generate this board:

![Arimaa-viz screenshot](arimaavizscreenshot.png "Arimaa-viz screenshot")

(Screenshot taken on my development machine. Hope it works for you too!)

Arimaa-viz came about because a friend and I decided to have an AI duel.
We discovered that Arimaa tools in JS are LACKING!
Arimaa-viz represents one small step towards the light.
Sure, it could use some options, like colourings, 
or changing the charset for pieces, or diffing boards and highlighting,
or accepting standard move syntax and generating new boards, or ... or ...
Sure, it could! For now, it works enough to accept a string and output a board.

(Hopefully it will be possible to make arimaa-viz the 'frontend' for existing implementations such as https://bitbucket.org/egaga/arimaa-game-viewer/ and https://github.com/Janzert/AEI
Next step will be streaming visualization of a bot-bot 'round robin'.)

### Progress
So roundrobin generates lots of end-game boards like this:
```
47g
 +-----------------+
8| . . . . . . . . |
7| . . . . . . . R |
6| . . x . . x . . |
5| . . . . . . . . |
4| . . . . . . . . |
3| . R x . . x . . |
2| D . . . . . . . |
1| . M d . . C . . |
 +-----------------+
   a b c d e f g h  
```
So a way of getting the 64-char string out must be found, or a converter must be written.

A crude patching of https://github.com/Janzert/AEI/blob/master/pyrimaa/roundrobin.py line 271 "print game.position.board_to_str()" should suffice for delivering JUST the boards.
Yes! board_to_str("short") is the answer! Edited for local testing at /usr/local/lib/python2.7/dist-packages/pyrimaa/roundrobin.py

After that, the sequence of states from game start to game finish.

After that, global domination?